(function() {
    const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

    let today = new Date(),
        dd = String(today.getDate()).padStart(2, "0"),
        mm = String(today.getMonth() + 1).padStart(2, "0"),
        yyyy = today.getFullYear(),
        nextYear = yyyy + 1,
        dayMonth = "12/23/",
        arstart = dayMonth + yyyy + " 19:15:00";

    today = mm + "/" + dd + "/" + yyyy;
    if (today > arstart) {
        arstart = dayMonth + nextYear;
    }

    const xmas = moment().isBetween(yyyy + '-12-23 19:15:00', yyyy + '-12-27');
    if (xmas == true) {
        document.getElementById("countdown").style.display = "none";
    } else {
        document.getElementById("stream").style.display = "none";
        document.getElementById("cast").style.display = "none";
        const countDown = new Date(arstart).getTime(),
            x = setInterval(function() {

                const now = new Date().getTime(),
                    distance = countDown - now;

                document.getElementById("days").innerText = Math.floor(distance / (day)),
                    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
                    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
                    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
            }, 0)
    }
}());